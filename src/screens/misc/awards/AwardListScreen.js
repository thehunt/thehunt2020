import React, { useState, useEffect } from 'react';
import { getPostsWithAward } from '../../../api/scoring';
import Feed from '../../../components/feed/Feed';

export default function AwardListScreen({route}){
  const [posts, setPosts] = useState([]);

  const updatePosts = () => {
    getPostsWithAward(route.params.award).then(setPosts);
  };

  useEffect(() => {
    updatePosts();
  }, []);

  return <Feed posts={posts} updateScreen={updatePosts} />;
}
