import React from 'react';
import { View } from 'react-native';
import { SCREEN_WIDTH, SCREEN_HEIGHT } from '../constants/Layout';

export default function SplashScreen() {
  return (
    <View style={{width: SCREEN_WIDTH, height: SCREEN_HEIGHT, backgroundColor: '#000'}} />
  )
}