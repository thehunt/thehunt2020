import React from 'react';
import { Button } from 'react-native-paper';

export default function ButtonWrapper(props) {
  return <Button {...props} />
}