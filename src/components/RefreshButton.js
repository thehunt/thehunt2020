import React from 'react';
import { ActivityIndicator, IconButton } from 'react-native-paper';
import { GeneralIconButton } from './IconButtons';

const RefreshButton = ({onPress, isLoading}) => isLoading ? (
  <ActivityIndicator style={{marginRight: 12}} />
): (
  <GeneralIconButton onPress={onPress} icon='refresh' size={24} />
);
export default RefreshButton;