import { SCREEN_WIDTH } from './Layout';

export const DEFAULT_IMG = require('../../assets/images/default_gray.png')
export const maxColumnWidth = 500
export const fullWidthPadding = SCREEN_WIDTH - 24 > maxColumnWidth ? maxColumnWidth : SCREEN_WIDTH - 24;
