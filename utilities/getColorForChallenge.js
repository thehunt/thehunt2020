export const getChallengeColor = category => {
  switch (category) {
  case 'solo': return '#ffd966';
  case 'team': return '#b460c6';
  case 'contest': return '#14f597';
  case 'war': return '#cc4125';
  case 'announcement': return '#fff';
  default: return 'transparent';
  }
};