import { Platform } from 'react-native';

export default function forPlatform(options) {
  switch (Platform.OS) {
  case 'ios':
    return options.ios;
  case 'android':
    return options.android;
  case 'web':
    return options.web;
  default: 
    return null;
  }
}